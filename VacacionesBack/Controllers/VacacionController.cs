﻿using Cygnus.Comunes.Clases;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using VacacionesBack.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Cygnus.Datos.RRHH;
using log4net;


namespace VacacionesBack.Controllers
{
    public class VacacionController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger("VacacionController");

        // GET: Vacacion
        static HttpClient client = new HttpClient();

        public ActionResult Index()
        {
            return View();
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult resumenVacacionesXLegajo(int legajo)
        {
            Status estado = new Status(HttpStatusCode.OK);
            ResumenVacaciones rv = new ResumenVacaciones();
            try
            {
                DataSet dsResumenVacaciones = new VacacionDA().TRAER_VACACIONES_X_LEGAJO(legajo);

                if ((dsResumenVacaciones != null) && (dsResumenVacaciones.Tables.Count > 0) && dsResumenVacaciones.Tables[0].Rows.Count > 0 && dsResumenVacaciones.Tables[1].Rows.Count > 0)
                {
                    rv = new ResumenVacaciones(dsResumenVacaciones.Tables[0].Rows[0]);

                    rv.cantidad_dias_tomados = dsResumenVacaciones.Tables[1].AsEnumerable().Sum(x => Convert.ToInt32(x["cantidadDiasOcupados"].ToString()));
                    rv.cantidad_dias_disponibles = dsResumenVacaciones.Tables[1].AsEnumerable().Sum(x => Convert.ToInt32(x["cantidadDiasDisponibles"].ToString()));
                    rv.cantidad_dias_progresivo = dsResumenVacaciones.Tables[1].AsEnumerable().Where(x => x["TipoDias"].ToString().Equals("PROGRESIVO")).Sum(x => Convert.ToInt32(x["cantidadDiasDisponibles"].ToString()));
                    rv.cantidad_papeletas_emitidas = Convert.ToInt32(dsResumenVacaciones.Tables[0].Rows[0]["cantidadPapeleta"].ToString());
                }
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: resumenVacacionesXLegajo");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al momento de obtener el resumen del legajo: " + legajo.ToString(), ex.Message);
            }
            
            return Json(new object[] { estado, rv }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult resumenDetalleVacacionesXLegajo(int legajo, string tipo)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<ResumenDetalleVacaciones> lista = new List<ResumenDetalleVacaciones>();

            try
            {
                DataSet dsResumenVacaciones = new VacacionDA().TRAER_VACACIONES_X_LEGAJO(legajo);

                if ((dsResumenVacaciones != null) && (dsResumenVacaciones.Tables.Count > 1) && dsResumenVacaciones.Tables[1].Rows.Count > 0)
                {
                    if (tipo.Equals("OCUPADOS"))
                        lista = dsResumenVacaciones.Tables[1].AsEnumerable().Where(x => Convert.ToInt32(x["cantidadDiasOcupados"].ToString()) > 0).Select(x => new ResumenDetalleVacaciones(x)).ToList();
                    else
                    {
                        if (tipo.Equals("DISPONIBLES"))
                            lista = dsResumenVacaciones.Tables[1].AsEnumerable().Where(x => Convert.ToInt32(x["cantidadDiasDisponibles"].ToString()) > 0).Select(x => new ResumenDetalleVacaciones(x)).ToList();
                        else
                            //PROGRESIVOS
                            lista = dsResumenVacaciones.Tables[1].AsEnumerable().Where(x => x["TipoDias"].ToString().Equals("PROGRESIVO")).Select(x => new ResumenDetalleVacaciones(x)).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: resumenDetalleVacacionesXLegajo");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al momento de obtener el detalle del legajo: " + legajo.ToString(), ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);

        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult resumenDetallePapeletaEmitidasXLegajo(int legajo)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<ResumenDetallePapeletaEmitidas> lista = new List<ResumenDetallePapeletaEmitidas>(); 

            try
            {
                DataSet dsResumenPapeletaVacaciones = new VacacionDA().TRAER_PAPELETAS_VACACIONES_X_LEGAJO(legajo);

                if ((dsResumenPapeletaVacaciones != null) && (dsResumenPapeletaVacaciones.Tables.Count > 0) && dsResumenPapeletaVacaciones.Tables[0].Rows.Count > 0)
                    lista = dsResumenPapeletaVacaciones.Tables[0].AsEnumerable().Select(x => new ResumenDetallePapeletaEmitidas(x)).ToList();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: resumenDetallePapeletaEmitidasXLegajo");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al la información de las papeletas del legajo: " + legajo.ToString(), ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        //[System.Web.Http.Authorize]
        [HttpGet]
        public FileResult verPapeleta(int id)
        {
            DataSet dsArchivoPapeleta = new VacacionDA().TRAER_PAPELETAS(id);

            string archivo = "";
            byte[] bytesArchivo = null;


            if ((dsArchivoPapeleta != null) && (dsArchivoPapeleta.Tables.Count > 0) && dsArchivoPapeleta.Tables[0].Rows.Count > 0)
            {
                int tipoPapeleta = int.Parse(dsArchivoPapeleta.Tables[0].Rows[0]["tipoPapeleta"].ToString());

                if(tipoPapeleta == 4785)
                {
                    archivo = dsArchivoPapeleta.Tables[0].Rows[0]["url"].ToString();
                    archivo = archivo.Replace("\\cyg-server\\", "");

                    string rutaArchivo = ConfigurationManager.AppSettings["rutaPapeletas"] + archivo + ".pdf";

                    bytesArchivo = System.IO.File.ReadAllBytes(rutaArchivo);
                }
                else
                {
                    archivo = dsArchivoPapeleta.Tables[0].Rows[0]["url"].ToString();
                    bytesArchivo = traeBPO(archivo);
                    //bytesArchivo = traeBPO("5ca22c2e0b1ac43cce80d66c");

                }                
            }
            return File(bytesArchivo, "application/pdf","papeleta"+id+".pdf");
        }


        public byte[] traeBPO(string id)
        {
            TraeDocumentoBPO tdBPO = new TraeDocumentoBPO();
            DocumentoBPO doc = new DocumentoBPO();
            doc = tdBPO.traeDocumento(id);
            byte[] bytes = new WebClient().DownloadData(doc.pdf);

            return bytes;
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult traerNotificiacionesXLegajo(int legajo)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<Notificacion> lista = new List<Notificacion>();

            try
            {
                DataSet dsNotificaciones = new VacacionDA().traerNotificiacionesXLegajo(legajo);

                lista = dsNotificaciones.Tables[0].Rows.OfType<DataRow>().
                        Select(x => new Notificacion()
                        {
                            idAutonum = x["idAutonum"].ToString(),
                            legajo = x["legajo"].ToString(),
                            texto = x["texto"].ToString(),
                            icono= x["icono"].ToString(),
                            fecha= x["fecha"].ToString()
                        }).ToList();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: resumenDetallePapeletaEmitidasXLegajo");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al la información de las papeletas del legajo: " + legajo.ToString(), ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }


    }
}
