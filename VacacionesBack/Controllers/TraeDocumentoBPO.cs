﻿using Cygnus.Comunes.Clases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace VacacionesBack.Controllers
{
    public class TraeDocumentoBPO
    {
        public DocumentoBPO traeDocumento(string idDocBPO)
        {
            Hashtable ht = new Hashtable();

            string sUrL = "https://cygnus.idok.cl/api/mxmls/show?mxml_id=" + idDocBPO;
            HttpWebRequest request = WebRequest.Create(sUrL) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjp7IiRvaWQiOiI1YjI5NDQyMDBiMWFjNDI5ODRjMTNlMWYifX0.QYRRsY4IBHewfCDdgdlBqy9EgxN65SRUqjbbss97FXk");

            string resp = "";
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                resp = reader.ReadToEnd();
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = 2073741824;
            //PlantillaBPO plantilla = new PlantillaBPO();
            return serializer.Deserialize<DocumentoBPO>(resp);
        }
    }
}