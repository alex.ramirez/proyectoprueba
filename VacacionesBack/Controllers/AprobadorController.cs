﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Cygnus.Datos.RRHH;
using VacacionesBack.Models;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using log4net;
using System.Net;
using Cygnus.Datos.Comun;
using System.Web.WebPages;

namespace VacacionesBack.Controllers
{
    public class AprobadorController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger("AprobadorController");
        
        DataSet ds;
        VacacionDA vacDatos = new VacacionDA();

        // GET: Aprobador
        public ActionResult Index()
        {
            //GuardarSolicitudEstandar("20200622 00:00", "20200630 23:59", 881, 1264);
            //ValidaSolicitudVacaciones("20200622 00:00", "20200630 23:59", 881);
            //BuscarSolicitudAprobacion(881);
            return View();
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult CargaDDLAprobadores(string empresa, int cuenta, int sistema)
        {
            List<KeyValuePair<string, string>> lista = new List<KeyValuePair<string, string>>();
            Status estado = new Status(HttpStatusCode.OK, "Información registrada");
            try
            {
                ds = vacDatos.USUARIOS_APROBADORES_Listar(empresa, cuenta, sistema);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    lista = ds.Tables[0].AsEnumerable().Select(x => new KeyValuePair<string, string>(x["LEGAJO"].ToString(), x["NOMBRE"].ToString())).ToList();

                ds.Dispose();
            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener la información de los aprobadores", ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult InsertarUsuariosXAprobadores(string lspersona, string lsaprobadores, string usuario)
        {
            string mensaje = string.Empty;
            bool respInsertar = false;
            int posEliminar = 0;
            string json = null;

            int legajoPersona = 0;
            int legajoAprobador = 0;
            //int legajoActualizador = 0;

            //var productos = "[{\"Legajo\":53503,\"LegajoAprobador\":177,\"Actualizador\":1264},{\"Legajo\":181526,\"LegajoAprobador\":43,\"Actualizador\":1264}]";

            Status estado = new Status(HttpStatusCode.OK, "Información registrada");

            try
            {
                var listPersonas = JsonConvert.DeserializeObject<List<PersonasXAprobadores>>(lspersona);                
                var listAprobadores = JsonConvert.DeserializeObject<List<PersonasXAprobadores>>(lsaprobadores);               
                var listUsuario = JsonConvert.DeserializeObject<List<PersonasXAprobadores>>(usuario);

                //PREGUNTA SI EL OBJETO TRAE ALGO O NO
                if (listPersonas.Count > 0 && listAprobadores.Count > 0 && listUsuario.Count > 0)
                {
                    foreach (PersonasXAprobadores personas in listPersonas)
                    {
                        if (!Int32.TryParse(personas.Legajo.ToString(), out legajoPersona))
                        {
                            posEliminar = personas.Legajo;
                            log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo de la persona no es numérico: " + personas.Legajo);
                        }

                        if (posEliminar > 0)
                        {
                            //SALE DE INMEDIATO Y MANDA MENSAJE CON LA LISTA DE LEGAJOS QUE NO SE PUDIERON INGRESAR O ELIMINAR
                            break;
                        }
                        else
                        {
                            //RECORRER LOS PARAMETROS REALIZANDO ESTAS ACCIONES
                            if (vacDatos.USUARIO_X_APROBADORES_Delete(personas.Legajo))
                            {
                                foreach (PersonasXAprobadores aprobadores in listAprobadores)
                                {
                                    if (!Int32.TryParse(aprobadores.LegajoAprobador.ToString(), out legajoAprobador))
                                    {
                                        respInsertar = false;
                                        log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo del aprobador no es numérico: " + aprobadores.LegajoAprobador);
                                        break;
                                    }
                                }

                                foreach (PersonasXAprobadores aprobadores in listAprobadores)
                                {
                                    respInsertar = vacDatos.USUARIO_X_APROBADORES_Insert(personas.Legajo, aprobadores.LegajoAprobador, listUsuario[0].Usuario);
                                }
                            }
                            else
                            {
                                posEliminar = personas.Legajo;
                            }

                            if (respInsertar.Equals(false))
                            {
                                //si no se insertaron, se eliminan nuevamente.
                                posEliminar = personas.Legajo;
                                vacDatos.USUARIO_X_APROBADORES_Delete(personas.Legajo);
                            }
                        }
                    }

                    //SI EXISTIERON ERRORES
                    if (posEliminar > 0)
                    {
                        //SALE DE INMEDIATO DEL CICLO Y MANDA MENSAJE CON LA LISTA DE LEGAJOS QUE NO SE PUDIERON INGRESAR O ELIMINAR
                        mensaje = "Estos legajos no se pudieron ingresar los aprobadores, por favor volver a intentar: ";
                        for (int i = posEliminar; i < listPersonas.Count; i++)
                        {
                            if (i == 0)
                                mensaje = mensaje + listPersonas[i].ToString();
                            else
                                mensaje = mensaje + "," + Environment.NewLine + listPersonas[i].ToString();
                        }

                        estado = new Status(HttpStatusCode.InternalServerError, mensaje);
                        return Json(estado, JsonRequestBehavior.AllowGet);
                    }

                    if(respInsertar)
                    {
                        return Json(estado, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(estado, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    estado = new Status(HttpStatusCode.InternalServerError, "Parámetros con información incorrecta");
                    return Json(estado, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //SI OCURRE UN ERROR CON EL JSON LO GUARDA EN EL LOG
                log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Mensaje: Problema al deserializar el siguiente JSON:" + json);
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al deserializar la información", ex.Message);
                return Json(estado, JsonRequestBehavior.AllowGet);
            }

            
            

            //foreach (PersonasXAprobadores personas in listPersonas)
            //{
            //    Console.WriteLine("Legajo: " + personas.Legajo);
            //}

            //foreach (PersonasXAprobadores aprobadores in listAprobadores)
            //{
            //    Console.WriteLine("LegajoAprobador: " + aprobadores.LegajoAprobador);
            //}

            //foreach (PersonasXAprobadores usr in listUsuario)
            //{
            //    Console.WriteLine("Usuario: " + usr.Usuario);
            //}

            //PersonasXAprobadores pxa = null;
            //Status estado = new Status(HttpStatusCode.OK, "Información registrada");
            //try
            //{
            //    //DESERIALIZA EL JSON QUE SE RECIBE
            //    pxa = JsonConvert.DeserializeObject<PersonasXAprobadores>(json);
            //}
            //catch (Exception ex)
            //{
            //    //SI OCURRE UN ERROR CON EL JSON LO GUARDA EN EL LOG
            //    log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " +  ex.Message.ToString() +"  | Mensaje: Problema al deserializar el siguiente JSON:" + json);
            //    estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al deserializar la información", ex.Message);
            //    return Json(estado, JsonRequestBehavior.AllowGet);
            //}

            //PREGUNTA SI EL OBJETO TRAE ALGO O NO
            //if (pxa != null)
            //{
            //    //SE VALIDA QUE EL LEGAJO ACTUALIZADOR SEA UN NUMERO Y SALE DEL SERVICIO DE INMEDIATO
            //    if (!Int32.TryParse(pxa.legajoActualizador.ToString(), out legajoActualizador))
            //    {
            //        log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo de la persona logeada no es numérico: " + pxa.legajoActualizador.ToString());
            //        estado = new Status(HttpStatusCode.InternalServerError, "El legajo de la persona que se encuentra conectada no es numérico. Legajo: " + pxa.legajoActualizador.ToString());
            //        return Json(estado, JsonRequestBehavior.AllowGet);
            //    }

            //    for (int lp = 0; lp < pxa.listaPersonas.Count; lp++)
            //    {
            //        if (!Int32.TryParse(pxa.listaPersonas[lp].ToString(), out legajoPersona))
            //        {
            //            posEliminar = lp;
            //            log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo de la persona no es numérico: " + pxa.listaPersonas[lp].ToString());
            //        }

            //        if (posEliminar > 0)
            //            //sale de inmediato y manda mensaje con la lista de legajos q no se pudieron ingresar o eliminar
            //            break;

            //        //recorrer los parametros e ir realizando estas acciones
            //        if (vacDatos.USUARIO_X_APROBADORES_Delete(legajoPersona))
            //        {
            //            for (int la = 0; la < pxa.listaAprobadores.Count; la++)
            //            {
            //                if (!Int32.TryParse(pxa.listaAprobadores[la].ToString(), out legajoAprobador))
            //                {
            //                    respInsertar = false;
            //                    log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo del aprobador no es numérico: " + pxa.listaAprobadores[la].ToString());
            //                    break;
            //                }

            //                respInsertar = vacDatos.USUARIO_X_APROBADORES_Insert(legajoPersona, legajoAprobador, legajoActualizador);

            //                if (!respInsertar)
            //                {
            //                    //no se insertó
            //                    log.Error("[" + DateTime.Now.ToLongDateString() + "] Mensaje: El legajo del aprobador no es numérico: " + pxa.listaAprobadores[la].ToString());
            //                    break;
            //                }
            //            }
            //        }
            //        else
            //            posEliminar = lp;

            //        if (!respInsertar)
            //        {
            //            //si no se insertaron, se eliminan nuevamente.
            //            posEliminar = lp;
            //            vacDatos.USUARIO_X_APROBADORES_Delete(legajoPersona);
            //        }
            //    }


            //    //si existieron errores
            //    if (posEliminar > 0)
            //    {
            //        //sale de inmediato del ciclo y manda mensaje con la lista de legajos q no se pudieron ingresar o eliminar
            //        mensaje = "Estos legajos no se pudieron ingresar los aprobadores, por favor volver a intentar: ";
            //        for (int i = posEliminar; i < pxa.listaPersonas.Count; i++)
            //        {
            //            if (i == 0)
            //                mensaje = mensaje + pxa.listaPersonas[i].ToString();
            //            else
            //                mensaje = mensaje + "," + Environment.NewLine + pxa.listaPersonas[i].ToString();
            //        }

            //        estado = new Status(HttpStatusCode.InternalServerError, mensaje);
            //    }
            //}
            //else
            //    estado = new Status(HttpStatusCode.InternalServerError, "Parámetros con información incorrecta");
            //return Json(estado, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult detallePersonasAprobar(string empresa, int ctaautoid, int legajo, string rut, string appaterno, string apmaterno, string nombre)
        {
            List<PersonasAprobar> lista = new List<PersonasAprobar>();
            Status estado = new Status(HttpStatusCode.OK, "Información registrada");

            List<PersonasAprobar> nLista = new List<PersonasAprobar>();
            PersonasAprobar personaAprobador;
            int legajo_bandera = 0;
            string lap = "";
            string title = "";
            string nap = "";

            try
            {
                DataSet dsPersonasAprobar = new VacacionDA().TRAER_PERSONAS_APROBAR(empresa, ctaautoid, legajo, rut, appaterno, apmaterno, nombre);

                if ((dsPersonasAprobar != null) && (dsPersonasAprobar.Tables.Count > 0) && dsPersonasAprobar.Tables[0].Rows.Count > 0)
                {
                    lista = dsPersonasAprobar.Tables[0].AsEnumerable().Select(x => new PersonasAprobar(x)).ToList();

                    for (int i = 0; i < lista.Count(); i++)
                    {
                        int cantidad = lista.Count(x => x.legajo == lista[i].legajo);
                        
                        if (lista[i].legajo != legajo_bandera)
                        {
                            legajo_bandera = lista[i].legajo;

                            personaAprobador = new PersonasAprobar();
                            personaAprobador.legajo = lista[i].legajo;
                            personaAprobador.empresa = lista[i].empresa;
                            personaAprobador.cuenta = lista[i].cuenta;
                            personaAprobador.fecha_ingreso = lista[i].fecha_ingreso;
                            personaAprobador.rut = lista[i].rut;
                            personaAprobador.nombre_completo = lista[i].nombre_completo;

                            if (lista[i].legajo_aprobador != "0")
                            {
                                for (int x = 0; x < cantidad; x++)
                                {
                                    lap += lista[x].legajo_aprobador + ",";
                                    title += lista[x].nombre_aprobador + ", ";
                                    nap += lista[x].nombre_aprobador + ",";
                                }

                                personaAprobador.nombre_aprobador += "<span style='font-size: 12px;white-space: nowrap;' title='" + title.Remove(title.Length -2) + "' value='" + lap + "'>";

                                for (int x = 0; x < cantidad; x++)
                                {
                                    personaAprobador.legajo_aprobador += lista[x].legajo_aprobador + ",";                                    
                                    //personaAprobador.nombre_aprobador += "<span style='font-size: 12px;' value='" + lista[x].legajo_aprobador + "'>" + lista[x].nombre_aprobador + " " + "</span><br/>";
                                    //personaAprobador.nombre_aprobador += lista[x].nombre_aprobador + " </br>";
                                }

                                personaAprobador.nombre_aprobador += nap.Split(',')[0].ToString() + "..." + " </br>";

                                personaAprobador.nombre_aprobador += "</span>";

                                personaAprobador.legajo_aprobador = personaAprobador.legajo_aprobador.Remove(personaAprobador.legajo_aprobador.Length -1);                                
                            }
                            else
                            {
                                personaAprobador.legajo_aprobador = lista[i].legajo_aprobador;
                                personaAprobador.nombre_aprobador = lista[i].nombre_aprobador;
                            }

                            nLista.Add(personaAprobador);
                        }
                    }

                    return Json(new object[] { estado, nLista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener la información de los aprobadores", ex.Message);

                return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
            }            
        }

        [HttpPost]
        public JsonResult PruebaMensajesErroresJson(int msj)
        {
            List<object[]> lista = new List<object[]>();
            switch (msj)
            {
                case 1:
                    lista.Add(new object[]{ HttpStatusCode.OK, "esta correcto 200" });
                    break;
                case 2:
                    lista.Add(new object[] { HttpStatusCode.InternalServerError, "InternalServerError 500" });
                    break;
                case 3:
                    lista.Add(new object[] { HttpStatusCode.NotFound, "NotFound 404" });
                    break;
                case 4:
                    lista.Add(new object[] { HttpStatusCode.Accepted, "Accepted 202" });
                    break;
                case 5:
                    lista.Add(new object[] { HttpStatusCode.BadRequest, "BadRequest 400" });
                    break;
                case 6:
                    lista.Add(new object[] { HttpStatusCode.GatewayTimeout, "GatewayTimeout 504" });
                    break;
                default:
                    break;
            }

            return Json(lista, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ValidaSolicitudVacaciones(string fechadesde, string fechahasta, int legajo)
        {
            List<Vacaciones> lista = new List<Vacaciones>();
            List<Vacaciones> ls = new List<Vacaciones>();
            Vacaciones vacaciones;

            Status estado = new Status(HttpStatusCode.OK, "Información registrada");
            int error; string glosa;

            try
            {
                ds = vacDatos.VALIDA_SOLICITUD_VACACIONES(fechadesde, fechahasta, legajo, out error, out glosa);
                if (error.Equals(0))
                {
                    ls = ds.Tables[0].AsEnumerable().Select(x => new Vacaciones(x)).ToList();

                    vacaciones = new Vacaciones();
                    vacaciones.FechaDesde = ls[0].FechaDesde;
                    vacaciones.FechaHasta = ls[0].FechaHasta;
                    vacaciones.CantidadDiasLaborales = ls[0].CantidadDiasLaborales;
                    vacaciones.CantidadSabadosyDomingos = ls[0].CantidadSabadosyDomingos;
                    vacaciones.CantidadFeriados = ls[0].CantidadFeriados;
                    vacaciones.CantidadFeriadosRegionales = ls[0].CantidadFeriadosRegionales;
                    lista.Add(vacaciones);

                    return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    estado = new Status(HttpStatusCode.ExpectationFailed, glosa);
                    return Json(new object[] { estado, glosa }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al guardar la solicitud", ex.Message);
                return Json(new object[] { estado, ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarSolicitudAprobacion(int legajo)
        {
            List<SolicitudAprobacion> lista = new List<SolicitudAprobacion>();
            Status estado = new Status(HttpStatusCode.OK, "Información registrada");

            List<SolicitudAprobacion> ls = new List<SolicitudAprobacion>();
            SolicitudAprobacion solicitudAprobacion;

            try
            {
                ds = vacDatos.BUSCAR_SOLICITUD_APROBACION(legajo);                

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ls = ds.Tables[0].AsEnumerable().Select(x => new SolicitudAprobacion(x)).ToList();

                    for (int i = 0; i < ls.Count(); i++)
                    {
                        solicitudAprobacion = new SolicitudAprobacion();
                        solicitudAprobacion.IdSolicitud = ls[i].IdSolicitud;
                        solicitudAprobacion.Empresa = ls[i].Empresa;
                        solicitudAprobacion.Rut = ls[i].Rut;
                        solicitudAprobacion.NombreCompleto = ls[i].NombreCompleto;
                        solicitudAprobacion.FechaInicioVacaciones = ls[i].FechaInicioVacaciones;
                        solicitudAprobacion.FechaTerminoVacaciones = ls[i].FechaTerminoVacaciones;
                        solicitudAprobacion.CantidadDias = ls[i].CantidadDias;
                        solicitudAprobacion.FechaAprobacion = ls[i].FechaAprobacion;
                        solicitudAprobacion.LegajoAprobador = ls[i].LegajoAprobador;
                        lista.Add(solicitudAprobacion);
                    }
                    ds.Dispose();
                }
                else
                {
                    return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al buscar la solicitud", ex.Message);
                return Json(estado, JsonRequestBehavior.AllowGet);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarSolicitudVacaciones(string fechadesde, string fechahasta, int legajo, int usuario)
        {
            Status estado = new Status(HttpStatusCode.OK, "Información registrada");
            int error; string glosa;

            try
            {
                ds = vacDatos.BUSCAR_JORNADA_X_LEGAJO(legajo, out error, out glosa);
                if (error.Equals(0))
                {
                    if (ds.Tables[0].Rows[0]["TIPO_JORNADA"].ToString() == "FULL")
                    {
                        vacDatos.GuardarSolicitudEstandar(fechadesde, fechahasta, legajo, usuario, out error, out glosa);
                        if (error.Equals(0))
                        {
                            return Json(estado, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            estado = new Status(HttpStatusCode.ExpectationFailed, glosa);
                            return Json(estado, glosa, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (ds.Tables[0].Rows[0]["TIPO_JORNADA"].ToString() != "FULL" || ds.Tables[0].Rows[0]["TIPO_JORNADA"].ToString() != "AR22")
                    {
                        vacDatos.GuardarSolicitudParcial(fechadesde, fechahasta, legajo, usuario, out error, out glosa);
                        if (error.Equals(0))
                        {
                            return Json(estado, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            estado = new Status(HttpStatusCode.ExpectationFailed, glosa);
                            return Json(estado, glosa, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        estado = new Status(HttpStatusCode.ExpectationFailed, "La jornada no se encontró");
                        return Json(estado, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    estado = new Status(HttpStatusCode.Gone, "Ha ocurrido un problema al obtener la jornada");
                    return Json(estado, glosa, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al guardar la solicitud", ex.Message);
                return Json(estado, JsonRequestBehavior.AllowGet);
            }
        }        

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult aprobarSolicitudVacaciones(int legajo,int idSolicitud)
        {
            Status estado = new Status(HttpStatusCode.OK, "Se aprobo correctamente la solicitud");

            try
            {
                int cantidadAprobadoresRestantes = new VacacionDA().aprobarSolicitudVacaciones(legajo,idSolicitud);

                if(cantidadAprobadoresRestantes == 0)
                {
                    DataSet datosSolicitante = new DataSet();
                    datosSolicitante = new VacacionDA().traerInformacionSolicitanteSolicitud(idSolicitud);
                    string mailSolicitante = datosSolicitante.Tables[0].Rows[0]["e_mail_per_lab"].ToString();
                    //string mailSolicitante = "alexander.mardones@cygnus.cl";

                    string asunto = "Aprobación Solicitud de vacaciones";
                    string cuerpo = "<html><HEAD></HEAD><BODY>";
                    cuerpo += "<h3>Hola " + datosSolicitante.Tables[0].Rows[0]["nombre"].ToString() + " " +datosSolicitante.Tables[0].Rows[0]["apellido"].ToString()+" "+ datosSolicitante.Tables[0].Rows[0]["apellido_materno"].ToString()+ "</h3>";
                    cuerpo += "<p>Se informa que su solicitud de vacaciones id: " + idSolicitud + " con fecha inicio: " + datosSolicitante.Tables[0].Rows[0]["fechaInicioVacaciones"].ToString();
                    cuerpo += " y fecha termino: " + datosSolicitante.Tables[0].Rows[0]["fechaTerminoVacaciones"].ToString() + ", por un total de " + datosSolicitante.Tables[0].Rows[0]["cantidadDias"].ToString() + " días se encuentra aprobada y disponible para su firma.</p>";
                    cuerpo += "</BODY></html>";


                    new MetodosComunesDA().EnviaMailHTML("sistemas@cygnus.cl",mailSolicitante,"",asunto,cuerpo,"",true);
                    
                    // al no tener mas aprobadores pendientes la solicitud
                    // viene todo el tema de la notificacion de aprobación, envio de papeleta
                    // el estado de la solicitud ya se cambio a aprobado en la bd

                }
                

                return Json(estado, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al intentar aprobar la solcitud", ex.Message);

                return Json(estado, JsonRequestBehavior.AllowGet);
            }
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult rechazarSolicitudVacaciones(int legajo, int idSolicitud,string comentario)
        {
            Status estado = new Status(HttpStatusCode.OK, "Se rechazo correctamente la solicitud");

            try
            {
                new VacacionDA().rechazarSolicitudVacaciones(legajo, idSolicitud,comentario);

                DataSet datosSolicitante = new DataSet();
                datosSolicitante = new VacacionDA().traerInformacionSolicitanteSolicitud(idSolicitud);
                string mailSolicitante = datosSolicitante.Tables[0].Rows[0]["e_mail_per_lab"].ToString();
                //string mailSolicitante = "alexander.mardones@cygnus.cl";

                string asunto = "Aprobación Solicitud de vacaciones";
                string cuerpo = "<html><HEAD></HEAD><BODY>";
                cuerpo += "<h3>Hola " + datosSolicitante.Tables[0].Rows[0]["nombre"].ToString() + " " + datosSolicitante.Tables[0].Rows[0]["apellido"].ToString() + " " + datosSolicitante.Tables[0].Rows[0]["apellido_materno"].ToString() + "</h3>";
                cuerpo += "<p>Se informa que su solicitud de vacaciones id: " + idSolicitud + " con fecha inicio: " + datosSolicitante.Tables[0].Rows[0]["fechaInicioVacaciones"].ToString();
                cuerpo += " y fecha termino: " + datosSolicitante.Tables[0].Rows[0]["fechaTerminoVacaciones"].ToString() + ", por un total de " + datosSolicitante.Tables[0].Rows[0]["cantidadDias"].ToString() + " días se encuentra Rechazada.";
                cuerpo += "</BODY></html>";

                new MetodosComunesDA().EnviaMailHTML("sistemas@cygnus.cl", mailSolicitante, "", asunto, cuerpo, "", true);


                //ya se cambio el estado de la solicitud a rechazada, deberian venir las notificaciones del rechazo


                return Json(estado, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al intentar rechazar la solcitud", ex.Message);

                return Json(estado, JsonRequestBehavior.AllowGet);
            }
        }        
    }
}