﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Cygnus.Datos.Comun;
using Cygnus.Datos.RRHH;
using VacacionesBack.Models;
using log4net;
using System.Net;

namespace VacacionesBack.Controllers
{
    public class CargarFiltrosController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger("CargarFiltrosController");

        DataSet ds;
        ParametroDA paramDatos = new ParametroDA();
        UsuarioDA usrDatos = new UsuarioDA();
        VacacionDA vacDatos = new VacacionDA();
        // GET: CargarFiltros
        public ActionResult Index()
        {
            return View();
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult legajosXRut(string rut)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<KeyValuePair<int, string>> lista = new List<KeyValuePair<int, string>>();

            try
            {
                ds = new Cygnus.Datos.RRHH.VacacionDA().TRAER_LEGAJOS_ACTIVOS_X_RUT(rut);

                if ((ds != null) && (ds.Tables.Count > 0) && ds.Tables[0].Rows.Count > 0)
                    lista = ds.Tables[0].AsEnumerable().Select(x => new KeyValuePair<int, string>(Convert.ToInt32(x["LEGAJO"].ToString()), x["EMPRESA"].ToString())).ToList();

                ds.Dispose();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: legajosXRut");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener los legajos del rut: " + rut, ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult CargaDDLParametrizados(int idTabla)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<KeyValuePair<string, string>> lista = new List<KeyValuePair<string, string>>();
            try
            {
                ds = paramDatos.PARAMETRO_Parametros_x_Tabla_ComboBox(idTabla, 3, string.Empty);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    lista = ds.Tables[0].AsEnumerable().Select(x => new KeyValuePair<string, string>(x["ID_PARAMETRO"].ToString(), x["DESCRIPCION"].ToString())).ToList();

                ds.Dispose();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: CargaDDLParametrizados");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener la información parametrizados de la tabla: " + idTabla.ToString(), ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult CargarDDLEmpresas(int uxs_autoid, string texto_inicial)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<KeyValuePair<string, string>> lista = new List<KeyValuePair<string, string>>();
            try
            {
                ds = vacDatos.EMPRESA_X_USUARIO_Listar(uxs_autoid, texto_inicial);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    lista = ds.Tables[0].AsEnumerable().Select(x => new KeyValuePair<string, string>(x["EMP_CODIGO"].ToString(), x["EMP_NOMBRE"].ToString())).ToList();

                ds.Dispose();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: CargarDDLEmpresas");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener la información de las empresas", ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult CargaDDLCuentas(int uxs_autoid, string texto_inicial)
        {
            Status estado = new Status(HttpStatusCode.OK);
            List<KeyValuePair<string, string>> lista = new List<KeyValuePair<string, string>>();
            try
            {
                ds = vacDatos.CUENTA_X_USUARIO_Listar(uxs_autoid, texto_inicial);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    lista = ds.Tables[0].AsEnumerable().Select(x => new KeyValuePair<string, string>(x["CTA_AUTOID"].ToString(), x["CTA_NOMBRE"].ToString())).ToList();

                ds.Dispose();
            }
            catch (Exception ex)
            {
                //log.Fatal("[" + DateTime.Now.ToLongDateString() + "] Error: " + ex.Message.ToString() + "  | Método: CargaDDLCuentas");
                estado = new Status(HttpStatusCode.InternalServerError, "Ha ocurrido un problema al obtener la información de las cuentas", ex.Message);
            }

            return Json(new object[] { estado, lista }, JsonRequestBehavior.AllowGet);
        }

    }
}