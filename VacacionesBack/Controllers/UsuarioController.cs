﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web.Mvc;
using VacacionesBack.Models;
using System.Web.Http.Cors;
//using System.Web.Http;

namespace VacacionesBack.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult validaUsuario(string usuario, string pass, int sistema)
        {
            ControlTrafico ct = new ControlTrafico();

            DataSet dsUsuario = new Cygnus.Datos.Comun.UsuarioDA().USUARIO_TraerXRut(usuario);
            
            if ((dsUsuario == null) || (dsUsuario.Tables.Count == 0) || dsUsuario.Tables[0].Rows.Count == 0)
            {
                ct.estadoSession = 0;
                ct.tokenSession = "";
                ct.mensajeSession = "El usuario ingresado no existe";
                ct.rutUsuarioSession = "";
                ct.usrUsuarioSession = "";
                ct.uxsUsuarioSession = "";
                ct.nombreUsuarioSession = "";
                return Json(ct, JsonRequestBehavior.AllowGet);
            }
            else
            {
                DataTable datosUsuario = dsUsuario.Tables[0];

                Usuario usuarioActivo = new Usuario();
                usuarioActivo.autoid = long.Parse(datosUsuario.Rows[0]["USR_AUTOID"].ToString());
                usuarioActivo.login = datosUsuario.Rows[0]["Login"].ToString();
                usuarioActivo.pass = datosUsuario.Rows[0]["Password"].ToString();
                usuarioActivo.nombre = datosUsuario.Rows[0]["Nombre"].ToString();
                usuarioActivo.rut = datosUsuario.Rows[0]["Rut"].ToString();
                usuarioActivo.estado = datosUsuario.Rows[0]["Estado"].ToString();
                usuarioActivo.mail = datosUsuario.Rows[0]["E-Mail"].ToString();

                if (pass == usuarioActivo.pass)
                {
                    DataSet dsSistema = new DataSet();

                    try
                    {
                        dsSistema = new Cygnus.Datos.Comun.UsuarioDA().Usuario_Traer_Sistemas(usuario, pass);
                    }
                    catch (Exception ex)
                    {
                        ct.estadoSession = 0;
                        ct.tokenSession = "";
                        ct.mensajeSession = ex.Message;
                        ct.rutUsuarioSession = "";
                        ct.usrUsuarioSession = "";
                        ct.uxsUsuarioSession = "";
                        ct.nombreUsuarioSession = "";
                        return Json(ct, JsonRequestBehavior.AllowGet);
                    }

                    if ((dsSistema == null) || (dsSistema.Tables.Count == 0) || dsSistema.Tables[0].Rows.Count == 0)
                    {
                        ct.estadoSession = 0;
                        ct.tokenSession = "";
                        ct.mensajeSession = "El usuario tiene los privilegios desactivados, contactese con el Administrador";
                        ct.rutUsuarioSession = "";
                        ct.usrUsuarioSession = "";
                        ct.uxsUsuarioSession = "";
                        ct.nombreUsuarioSession = "";
                    }
                    else
                    {
                        DataRow[] foundRowsUXS = dsSistema.Tables[0].Select("sis_autoid = " + sistema.ToString());
                        if (foundRowsUXS.Length > 0)
                        {
                            ct.estadoSession = 1;
                            ct.tokenSession = GeneraToken.GenerarTokenJWT(usuarioActivo);
                            ct.mensajeSession = "OK";
                            ct.rutUsuarioSession = usuarioActivo.rut;
                            ct.usrUsuarioSession = usuarioActivo.autoid.ToString();
                            ct.uxsUsuarioSession = foundRowsUXS[0]["UXS_AUTOID"].ToString();
                            ct.nombreUsuarioSession = usuarioActivo.nombre;

                        }
                        else
                        {
                            ct.estadoSession = 0;
                            ct.tokenSession = "";
                            ct.mensajeSession = "El usuario no tiene acceso al sistema, contactese con el Administrador";
                            ct.rutUsuarioSession = "";
                            ct.usrUsuarioSession = "";
                            ct.uxsUsuarioSession = "";
                            ct.nombreUsuarioSession = "";
                        }
                    }
                }
                else
                {
                    ct.estadoSession = 0;
                    ct.tokenSession = "";
                    ct.mensajeSession = "La contraseña ingresada no es valida";
                    ct.rutUsuarioSession = "";
                    ct.usrUsuarioSession = "";
                    ct.uxsUsuarioSession = "";
                    ct.nombreUsuarioSession = "";
                }

            }

            return Json(ct, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.Authorize]
        [HttpPost]
        public JsonResult obtenerMenuJson(string usuariosistema)
        {
            List<Menu> menu = new List<Menu>();
            Menu aux;

            if (!string.IsNullOrEmpty(usuariosistema))
            {
                DataSet dsOpciones = new Cygnus.Datos.Comun.UsuarioDA().Usuario_Traer_Opciones_Menu_UXS(int.Parse(usuariosistema));

                DataTable dtOpciones = dsOpciones.Tables[0];

                List<OpcionMenu> datos = dtOpciones.Rows.OfType<DataRow>().
                                    Select(x => new OpcionMenu()
                                    {
                                        opcionAutoid = long.Parse(x["OP_AUTOID"].ToString()),
                                        opcionDescripcion = x["Opcion"].ToString(),
                                        opcionUrl = x["OP_URL"].ToString(),
                                        opcionToolTip = x["OP_TOOLTIP"].ToString(),
                                        opcionIconUrl = x["OP_ICON_URL"].ToString(),
                                        opcionItemPadre = int.Parse(string.IsNullOrEmpty(x["OP_ITEM_PADRE"].ToString()) ? "0" : x["OP_ITEM_PADRE"].ToString()),
                                        opcionOrden = int.Parse(string.IsNullOrEmpty(x["OP_ORDEN"].ToString()) ? "0" : x["OP_ORDEN"].ToString())
                                    }).ToList();

                var cabecerasMenu = datos.Where(s => s.opcionItemPadre == 0).Select(s => s);

                foreach (OpcionMenu a in cabecerasMenu)
                {
                    aux = new Menu();
                    aux.opcionAutoid = a.opcionAutoid;
                    aux.opcionDescripcion = a.opcionDescripcion;
                    aux.opcionUrl = a.opcionUrl;
                    aux.opcionToolTip = a.opcionToolTip;
                    aux.opcionIconUrl = a.opcionIconUrl;
                    aux.opcionItemPadre = a.opcionItemPadre;
                    aux.opcionOrden = a.opcionOrden;
                    aux.hijosMenu = datos.Where(x => x.opcionItemPadre == a.opcionAutoid).Select(x => x).ToList();

                    menu.Add(aux);
                }
            }

            return Json(menu, JsonRequestBehavior.AllowGet);
        }

    }
}