﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class ResumenVacaciones
    {
        public string nombre_completo { get; set; }
        public string codigo_empresa { get; set; }
        public string empresa { get; set; }
        public string codigo_estructura_liquida { get; set; }
        public string estructura_liquida { get; set; }
        public string fecha_ingreso { get; set; }
        public string rut { get; set; }
        public string estado { get; set; }
        public string ley { get; set; }
        public string correo_personal { get; set; }
        public string jornada { get; set; }
        public int cantidad_dias_tomados { get; set; }
        public int cantidad_dias_disponibles { get; set; }
        public int cantidad_papeletas_emitidas { get; set; }
        public int cantidad_dias_progresivo { get; set; }

        public ResumenVacaciones() { }
        public ResumenVacaciones(DataRow dr)
        {
            this.nombre_completo = dr["nombre_completo"].ToString().Trim();
            this.codigo_empresa = dr["emp_codigo"].ToString().Trim();
            this.empresa = dr["empresa"].ToString().Trim();
            this.codigo_estructura_liquida = dr["estr_liq"].ToString().Trim();
            this.estructura_liquida = dr["nombre_estr_liq"].ToString().Trim();
            this.fecha_ingreso = Convert.ToDateTime(dr["fecha_ingreso"].ToString()).ToShortDateString().Trim();
            this.rut = dr["cuil"].ToString().Trim();
            this.estado = dr["estado"].ToString().Trim();
            this.ley = dr["LEY21015"].ToString().Trim();
            this.correo_personal = dr["correo_personal"].ToString().Trim();
            this.jornada = dr["tipo_jornada"].ToString().Trim();
        }
    }
}