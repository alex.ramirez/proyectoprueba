﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class Vacaciones
    {
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
        public int CantidadDiasLaborales { get; set; }
        public int CantidadSabadosyDomingos { get; set; }
        public int CantidadFeriados { get; set; }
        public int CantidadFeriadosRegionales { get; set; }

        public Vacaciones() { }

        public Vacaciones(DataRow dr)
        {
            this.FechaDesde = dr["FECHADESDE"].ToString().Trim();
            this.FechaHasta = dr["FECHAHASTA"].ToString().Trim();
            this.CantidadDiasLaborales = Convert.ToInt32(dr["CANTIDADDIASLABORALES"]);
            this.CantidadSabadosyDomingos = Convert.ToInt32(dr["CANTIDADSABADOSYDOMINGOS"]);
            this.CantidadFeriados = Convert.ToInt32(dr["CANTIDADDIASFERIADOS"]);
            this.CantidadFeriadosRegionales = Convert.ToInt32(dr["CANTIDAD_FERIADOS_REGIONALES"]);
        }
    }
}