﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class SolicitudAprobacion
    {
        public int IdSolicitud { get; set; }
        public string Empresa { get; set; }
        public string Rut { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Nombre { get; set; }
        public string NombreCompleto { get; set; }
        public string FechaInicioVacaciones { get; set; }
        public string FechaTerminoVacaciones { get; set; }
        public int CantidadDias { get; set; }
        public string FechaAprobacion { get; set; }
        public string Comentario { get; set; }
        public int? LegajoAprobador { get; set; }

        public SolicitudAprobacion() { }

        public SolicitudAprobacion(DataRow dr)
        {
            this.IdSolicitud = Convert.ToInt32(dr["IDSOLICITUD"]);
            this.Empresa = dr["EMPRESA"].ToString().Trim();
            this.Rut = dr["RUT"].ToString().Trim();
            this.Rut = this.Rut.Insert(this.Rut.Length - 1, "-");
            this.NombreCompleto = dr["NOMBRE"].ToString().Trim() + " " + dr["APELLIDO_PATERNO"].ToString().Trim() + " " + dr["APELLIDO_MATERNO"].ToString().Trim();
            this.FechaInicioVacaciones = Convert.ToDateTime(dr["FECHAINICIOVACACIONES"].ToString()).ToShortDateString().Trim();
            this.FechaTerminoVacaciones = Convert.ToDateTime(dr["fechaTerminoVacaciones"].ToString()).ToShortDateString().Trim();
            this.CantidadDias = Convert.ToInt32(dr["CANTIDADDIAS"]);
            this.FechaAprobacion = dr["FECHAAPROBACION"].ToString() != "" ? Convert.ToDateTime(dr["FECHAAPROBACION"].ToString()).ToShortDateString().Trim() : null;
            this.Comentario = dr["COMENTARIO"].ToString().Trim();
            this.LegajoAprobador = dr["LEGAJO_APROBADOR"].ToString() != null ? Convert.ToInt32(dr["LEGAJO_APROBADOR"]) : (int?)null;
        }
    }
}