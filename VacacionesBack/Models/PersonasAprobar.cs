﻿using System;
using System.Data;

namespace VacacionesBack.Models
{
    public class PersonasAprobar
    {
        public int legajo { get; set; }
        public string empresa { get; set; }
        public string fecha_ingreso { get; set; }
        public string rut { get; set; }
        public string nombre_completo { get; set; }
        //public string apellido_paterno { get; set; }
        //public string apellido_materno { get; set; }
        //public string nombre { get; set; }
        public int cuenta { get; set; }
        public string legajo_aprobador { get; set; }
        public string nombre_aprobador { get; set; }

        public PersonasAprobar() { }
        public PersonasAprobar(DataRow dr)
        {
            this.legajo = Convert.ToInt32(dr["legajo"].ToString().Trim());
            this.empresa = dr["empresa"].ToString().Trim();            
            this.fecha_ingreso = Convert.ToDateTime(dr["fecha_ingreso"].ToString()).ToShortDateString().Trim();
            this.rut = dr["rut"].ToString().Trim();
            this.rut = this.rut.Insert(this.rut.Length - 1, "-");
            this.nombre_completo = dr["nombre"].ToString().Trim() + " " + dr["apellido_paterno"].ToString().Trim() + " " + dr["apellido_materno"].ToString().Trim();
            //this.apellido_paterno = dr["apellido_paterno"].ToString().Trim();
            //this.apellido_materno = dr["apellido_materno"].ToString().Trim();
            //this.nombre = dr["nombre"].ToString().Trim();
            this.cuenta = Convert.ToInt32(dr["cuenta"].ToString().Trim());
            //this.legajo_aprobador = Convert.ToInt32( string.IsNullOrEmpty(dr["legajo_aprobador"].ToString().Trim()) ? "0": dr["legajo_aprobador"].ToString().Trim() );
            this.legajo_aprobador = string.IsNullOrEmpty(dr["legajo_aprobador"].ToString().Trim()) ? "0" : dr["legajo_aprobador"].ToString().Trim();
            this.nombre_aprobador = string.IsNullOrEmpty(dr["nombre_aprobador"].ToString().Trim())? "": dr["nombre_aprobador"].ToString().Trim();
        }
    }
}