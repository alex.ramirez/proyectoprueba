﻿using System.Collections.Generic;

namespace VacacionesBack.Models
{
    public class Menu
    {
        public long opcionAutoid { get; set; }
        public string opcionDescripcion { get; set; }
        public string opcionUrl { get; set; }
        public string opcionToolTip { get; set; }
        public string opcionIconUrl { get; set; }
        public int opcionItemPadre { get; set; }
        public int opcionOrden { get; set; }

        public List<OpcionMenu> hijosMenu { get; set; }
    }
}