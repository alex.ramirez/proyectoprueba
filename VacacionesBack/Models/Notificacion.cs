﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class Notificacion
    {
        public string idAutonum { get; set; }
        public string legajo { get; set; }

        public string texto { get; set; }
        public string icono { get; set; }
        public string fecha { get; set; }

    }
}