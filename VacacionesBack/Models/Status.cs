﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace VacacionesBack.Models
{
    public class Status
    {
        public HttpStatusCode codigo { get; set; }
        public string mensaje { get; set; }
        public string excepcion { get; set; }

        public Status(){ }
        public Status(HttpStatusCode codigo)
        {
            this.codigo = codigo;
            this.mensaje = string.Empty;
            this.excepcion = string.Empty;
        }
        public Status(HttpStatusCode codigo, string mensaje) 
        {
            this.codigo = codigo;
            this.mensaje = mensaje;
            this.excepcion = string.Empty;
        }

        public Status(HttpStatusCode codigo, string mensaje, string excepcion)
        {
            this.codigo = codigo;
            this.mensaje = mensaje;
            this.excepcion = excepcion;
        }
    }
}