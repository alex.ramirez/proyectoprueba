﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace VacacionesBack.Models
{
    public class ResumenDetallePapeletaEmitidas
    {
        public int id_papeleta { get; set; }
        public int id_tipo_papeleta { get; set; }
        public string tipo_papeleta { get; set; }
        public string fecha_inicio_vacaciones { get; set; }
        public string fecha_termino_vacaciones { get; set; }
        public int cantidad_dias_totales { get; set; }
        public int id_estado_papeleta { get; set; }
        public string estado_papeleta { get; set; }
        public int cantidad_dias_legales { get; set; }
        public int cantidad_dias_esp_y_prog { get; set; }

        public ResumenDetallePapeletaEmitidas() { }
        public ResumenDetallePapeletaEmitidas(DataRow dr)
        {
            this.id_papeleta = Convert.ToInt32(dr["idPapeleta"].ToString().Trim());
            this.id_tipo_papeleta = Convert.ToInt32(dr["tipoPapeleta"].ToString().Trim());
            this.tipo_papeleta = dr["descripcionTipoPapeleta"].ToString().Trim();
            this.fecha_inicio_vacaciones = Convert.ToDateTime(dr["fechaInicioVacaciones"].ToString()).ToShortDateString().Trim();
            this.fecha_termino_vacaciones = Convert.ToDateTime(dr["FechaTerminoVacaciones"].ToString()).ToShortDateString().Trim();
            this.cantidad_dias_totales = Convert.ToInt32(dr["cantidadDias"].ToString().Trim());
            this.id_estado_papeleta = Convert.ToInt32(dr["IdEstadoPapeleta"].ToString().Trim());
            this.estado_papeleta = dr["estadoPapeleta"].ToString().Trim();
            this.cantidad_dias_legales = Convert.ToInt32(dr["Legales"].ToString().Trim());
            this.cantidad_dias_esp_y_prog = Convert.ToInt32(dr["progresivosNegociados"].ToString().Trim());
        }
    }
}