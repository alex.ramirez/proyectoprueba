﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    //public class PersonasXAprobadores
    //{
    //    public List<string> listaPersonas { get; set; }
    //    public List<string> listaAprobadores { get; set; }
    //    public string legajoActualizador { get; set; }

    //    public PersonasXAprobadores(){}
    //}

    public class PersonasXAprobadores
    {
        public int Legajo { get; set; }
        public int LegajoAprobador { get; set; }
        public int Usuario { get; set; }
    }
}