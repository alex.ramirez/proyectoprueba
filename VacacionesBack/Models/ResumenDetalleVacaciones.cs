﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace VacacionesBack.Models
{
    public class ResumenDetalleVacaciones
    {
        public int cantidad_dias_ocupados { get; set; }
        public int cantidad_dias_disponibles { get; set; }
        public int id_tipo_dias { get; set; }
        public string tipo_dias { get; set; }
        public string inicio_periodo { get; set; }
        public string fin_periodo { get; set; }
        public int id_periodo { get; set; }

        public ResumenDetalleVacaciones() { }
        public ResumenDetalleVacaciones(DataRow dr)
        {
            this.cantidad_dias_ocupados = Convert.ToInt32(dr["cantidadDiasOcupados"].ToString().Trim());
            this.cantidad_dias_disponibles = Convert.ToInt32(dr["cantidadDiasDisponibles"].ToString().Trim());
            this.id_tipo_dias = Convert.ToInt32(dr["idTipoDias"].ToString().Trim());
            this.tipo_dias = dr["TipoDias"].ToString().Trim();
            this.inicio_periodo = Convert.ToDateTime(dr["inicioPeriodo"].ToString()).ToShortDateString().Trim();
            this.fin_periodo = Convert.ToDateTime(dr["finPeriodo"].ToString()).ToShortDateString().Trim();
            this.id_periodo = Convert.ToInt32(dr["idPeriodo"].ToString().Trim());
        }
    }
}