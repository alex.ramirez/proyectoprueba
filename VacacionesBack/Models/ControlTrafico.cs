﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class ControlTrafico
    {
        public int estadoSession { get; set; }
        public string tokenSession { get; set; }
        public string mensajeSession { get; set; }
        public string rutUsuarioSession { get; set; }
        public string usrUsuarioSession { get; set; }
        public string uxsUsuarioSession { get; set; }
        public string nombreUsuarioSession { get; set; }
    }
}