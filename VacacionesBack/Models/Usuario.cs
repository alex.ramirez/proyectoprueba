﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VacacionesBack.Models
{
    public class Usuario
    {
        public long autoid { get; set; }
        public string login { get; set; }
        public string pass { get; set; }
        public string nombre { get; set; }
        public string rut { get; set; }
        public string estado { get; set; }
        public string mail { get; set; }
    }
}